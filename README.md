# HPC-lab

## Description
This is the exam project for UniMORE High Performance Computing course.
<br><br>
The goal is to minimize the execution time of the DynProg solver of the Polybench C library through:
- code rewriting
- parallelization with
    - OpenMP
    - Cuda
    - HLS
- performance analysis and bottleneck research.

## OpenMP

This folder contains the sources of the first assignment:
- the rewritten DynProg `rew_dynprog.c`
- the OpenMP parallelization of the rewritten DynProg `parallel_dynprog.c`
- the OpenMP parallelization over Nvidia Jetson Nano GPU of the rewritten DynProg `gpu_dynprog.c`

### Visuals
`OpenMP` folder contains also the presentation of the results of the first assignment.

### Usage
Into each subdir run
```bash
make EXT_CFLAGS="-DPOLYBENCH_TIME -DPOLYBENCH_DUMP_ARRAYS" clean all run
```
You can also specify the dataset size, for example
```bash
make EXT_CFLAGS="-DLARGE_DATASET -DPOLYBENCH_TIME -DPOLYBENCH_DUMP_ARRAYS" clean all run
```
Before running this commands, to compile `gpu_dynprog.c`, you must run
```bash
module load clang/11.0.0 cuda/10.0
```

## Authors
Gianluca Siligardi.

## License
MIT License.